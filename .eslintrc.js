module.exports = {
    "extends": ["node"],
    "env": {
        "browser": true,
        "es6": true,
        "node": true
    },
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "rules": {
        "commonjs": 0,
        "indent": [
            "warn",
            4
        ],
        "linebreak-style": [
            "error",
            "windows"
        ],
        "semi": [
            "error",
            "never"
        ],
        "no-unused-vars": [
            "warn",
            { "args": "none" }
        ],
        "no-console": [ 0 ],
        "guard-for-in": "warn",
        "comma-dangle": "error",
        "max-len": [ "error", { "code": 140 } ],
        "global-require": 2,
        "no-param-reassign": 0,
        "no-underscore-dangle": 0
    }
};
