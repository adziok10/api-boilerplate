const axios = require('axios');

//TODO configure jest

let token = '';
describe('Testing user endpoint', () => {
    
    test('POST /api/user/new (register) using async/await', async () => {
        const data = await axios.post('http://localhost:3000/api/user/new', {
            login: "testname",
            password: "zaq1@WSX",
            email: "test@test.com"
        })
        expect(data.data.status).toBeDefined()
        expect(data.data.status).toMatch('success')
    })

    test('POST /api/user (login) using async/await', async () => {
        const data = await axios.post('http://localhost:3000/api/user', {
            login: "testname",
            password: "zaq1@WSX"
        })
        expect(data.data.token).toBeDefined()
        token = data.data.token
        expect(data.data.status).toMatch('success')
    })

    test('PUT /api/user (update) using async/await', async () => {
        const data = await axios.put('http://localhost:3000/api/user', {
            password: "zaq1@WSX"
        })
        expect(data.data.status).toBeDefined()
        expect(data.data.status).toMatch('success')
    })

    test('DELETE /api/user (delete) using async/await', {}, async () => {
        const data = await axios.delete('http://localhost:3000/api/user')
        expect(data.data.status).toBeDefined()
        expect(data.data.status).toMatch('success')
    })

})