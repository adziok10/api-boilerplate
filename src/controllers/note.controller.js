
const Note = require('../models/note.model')

module.exports = {
    createNewNote(req, res) {

        const note = new Note({
            notebookId: req.body.notebook_id,
            title: req.body.title,
            content: req.body.content,
            ownerId: req.jwt_data.id
        })

        note.save()
            .then(data => res.status(201).json({
                status: 'success',
                data
            }))
            .catch(err => res.status(500).json(err))

    },
    getNoteByNotebookId(req, res) {

        Note.find({ notebookId: req.params.id, ownerId: req.jwt_data.id })
            .then(notes => !notes ? res.status(404).json({
                status: 'not found'
            }) : res.json({
                status: 'success',
                data: notes
            }))
            .catch(err => res.status(500).json(err))

    },
    getNoteById(req, res) {

        Note.findOne({ _id: req.params.id, ownerId: req.jwt_data.id })
            .then(note => !note ? res.status(404).json({
                status: 'not found'
            }) : res.json({
                status: 'success',
                data: note
            }))
            .catch(err => res.status(500).json(err))

    },
    deleteNoteById(req, res) {

        Note.findOneAndRemove({
            _id: req.params.id,
            ownerId: req.jwt_data.id
        })
            .then(info => res.json({
                status: "success"
            }))
            .catch(err => res.status(500).json(err))

    },
    updateNoteById(req, res) {

        const note = {
            title: req.body.title,
            content: req.body.content
        }

        Note.findOneAndUpdate({
            _id: req.params.id,
            ownerId: req.jwt_data.id
        }, note)
            .then(info => res.json({
                status: "success"
            }))
            .catch(err => res.status(500).json(err))

    }
}