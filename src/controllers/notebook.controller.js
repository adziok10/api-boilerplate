const Notebook = require('../models/notebook.model')

module.exports = {
    createNewNotebook(req, res) {

        const notebook = new Notebook({
            name: req.body.name,
            description: req.body.description,
            ownerId: req.jwt_data.id,
            color: req.body.color || '#ededaa'
        })

        notebook.save()
            .then(data => res.status(201).json({
                status: 'success',
                data
            }))
            .catch(err => res.status(500).json(err))

    },
    getNotebookList(req, res) {

        Notebook.find({ ownerId: req.jwt_data.id })
            .then(notebooks => {
                !notebooks ? res.status(404).json({
                    status: 'not found'
                }) : res.json(notebooks)
            })
            .catch(err => res.status(500).json(err))

    },
    getNotebookById(req, res) {

        Notebook.findOne({ _id: req.params.id, ownerId: req.jwt_data.id })
            .then(notebook => !notebook ? res.status(404).json({
                status: 'not found'
            }) : res.json(notebook))
            .catch(err => res.status(500).json(err))

    },
    deleteNotebook(req, res) {

        Notebook.findOneAndRemove({
                _id: req.params.id,
                ownerId: req.jwt_data.id
            })
            .then(info => res.json(info))
            .catch(err => res.status(500).json(err))

    },
    updateNotebook(req, res) {

        const notebook = req.body

        Notebook.findOneAndUpdate({
                _id: req.params.id,
                ownerId: req.jwt_data.id
            }, notebook)
            .then(info => res.json({
                status: "success"
            }))
            .catch(err => res.status(500).json(err))

    }
}