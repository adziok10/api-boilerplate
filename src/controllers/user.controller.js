const jwt = require('jsonwebtoken')

const User = require('../models/user.model')

module.exports = {
    getStatus(req, res) {
        return res.status(200).json({
            status: 'success',
            token: res.locals.token,
            expiresIn: 24 * 60 * 60 * 1000,
            email: req.jwt_data.email,
            _id: req.jwt_data._id
        })

    },
    register(req, res) {

        const user = new User({
            login: req.body.login,
            email: req.body.email,
            password: req.body.password,
            createAt: Date.now()
        })

        user.save()
            .then(data => res.status(201).json({
                status: "success"
            }))
            .catch(err => res.status(500).json(err))

    },
    login(req, res) {
        User.findOne({
            email: req.body.email
        })
            .then(record => {
                if (!record) return res.status(401).json({
                    'message': 'Bad login or password'
                })

                if (!record.isValidPassword(req.body.password)) return res.status(401).json({
                    'message': 'Bad login or password'
                })

                const payload = {
                    id: record._id,
                    name: record.login,
                    email: record.email
                }

                const token = jwt.sign(payload, 'adsadsasddas', {
                    expiresIn: 24 * 60 * 60 * 1000
                })

                return res.status(200).json({
                    status: 'success',
                    token,
                    expiresIn: 24 * 60 * 60 * 1000,
                    email: record.email,
                    _id: record._id
                })

            })
            .catch(err => res.status(500).json({
                'message': 'Server error',
                err
            }))

    },
    delete(req, res) {

        User.findOneAndRemove({
            _id: req.jwt_data.id
        })
            .then(info => res.status(201).json({
                status: "success"
            }))
            .catch(err => res.status(500).json(err))

    },
    update(req, res) {
        let user

        if (req.body.email && !req.body.password) user = {
            email: req.body.email
        }
        else if (req.body.password && !req.body.email) user = {
            password: req.body.password
        }
        else user = {
            password: req.body.password,
            email: req.body.email
        }

        User.findOneAndUpdate({
            _id: req.jwt_data.id
        }, user)
            .then(info => res.status(200).json({
                status: "success"
            }))
            .catch(err => res.status(500).json(err))

    }
}