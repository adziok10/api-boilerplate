const jwt = require('jsonwebtoken')

module.exports = ( req, res, next ) => {
    try {
        const token =  req.headers.authorization.split(" ")[1]
        const decoded = jwt.verify( token, 'adsadsasddas' )
        req.jwt_data = decoded
        res.locals.token = token
        next()
    } catch ( error ) {
        return res.status( 401 ).json({
            message: 'Auth failed'
        })
    }
}