const Ajv = require('ajv')

const ajv = new Ajv()

module.exports = (schema, data) => {
    return (req, res, next) => {
        const valid = ajv.validate(require(`../validators/${schema}.validator.js`)[data], req.body)
        return valid ? next() : next(ajv.errors)
    }
}