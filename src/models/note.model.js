const mongoose = require('mongoose')

const Schema = mongoose.Schema


const NoteSchema = new Schema({
    notebookId: {
        type: String,
        required: true
    },
    ownerId: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    content: {
        type: String,
        required: true
    }
})

NoteSchema.set('timestamps', true)

// NoteSchema.pre('save', next => {

//     this.updatedAt = new Date()

//     next()

// })

const NoteModel = mongoose.model( 'note', NoteSchema )


module.exports = NoteModel