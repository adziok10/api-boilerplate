const mongoose = require( 'mongoose' )

const Schema = mongoose.Schema


const NotebookSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    color: {
        type: String,
        default: "#ededaa"
    },
    ownerId: {
        type: String,
        require: true
    }
})

NotebookSchema.set('timestamps', true)

const NotebookModel = mongoose.model( 'notebook', NotebookSchema )


module.exports = NotebookModel