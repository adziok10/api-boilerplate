const mongoose = require( 'mongoose' )
const bcrypt = require( 'bcryptjs' )

const Schema = mongoose.Schema


const UserSchema = new Schema({
    login: {
        type: String,
        required: true,
        unique: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    }
})

UserSchema.set('timestamps', true)

UserSchema.pre( 'save', async function ( next ) {

    await bcrypt.hash( this.password, 10 )
        .then( hash => this.password = hash )

    next()
})

UserSchema.pre( 'findOneAndUpdate', async function ( next ) {
    console.log(this._update.password)
    await bcrypt.hash( this._update.password, 10 )
        .then( hash => this._update.password = hash )

    next()
})

UserSchema.methods.isValidPassword = function ( password ) {
    const user = this

    if ( bcrypt.compareSync( password, user.password )) 
        return true

    return false
}

const UserModel = mongoose.model( 'user', UserSchema )


module.exports = UserModel