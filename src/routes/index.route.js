const express = require('express')

const userRouter = require('./user.route')
const noteRouter = require('./note.route')
const notebookRouter = require('./notebook.route')

const router = express.Router()

router.get('/', (req, res) => res.send('elo'))

router.use('/user', userRouter)

router.use('/notebook', notebookRouter)

router.use('/note', noteRouter)

module.exports = router