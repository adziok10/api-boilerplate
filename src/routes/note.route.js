const express = require('express')

const asyncMiddleware = require('../lib/async-middleware.lib')

const NoteController = require('../controllers/note.controller')

const checkAuth = require('../lib/auth-check.lib');
// const validate = require('../lib/validate.lib');

const router = express.Router()

router.get('/list/:id' , checkAuth, asyncMiddleware( NoteController.getNoteByNotebookId ))

router.get( '/:id', checkAuth, asyncMiddleware( NoteController.getNoteById ))

router.post( '/', checkAuth, asyncMiddleware( NoteController.createNewNote ))

router.delete( '/:id', checkAuth, asyncMiddleware( NoteController.deleteNoteById ))

router.put( '/:id', checkAuth, asyncMiddleware( NoteController.updateNoteById ))

module.exports = router
