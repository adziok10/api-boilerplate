const express = require('express')

const asyncMiddleware = require('../lib/async-middleware.lib')

const NotebookController = require('../controllers/notebook.controller')

const checkAuth = require('../lib/auth-check.lib');
// const validate = require('../lib/validate.lib');

const router = express.Router()

router.get('/:id' , checkAuth, asyncMiddleware( NotebookController.getNotebookById ))

router.get( '/', checkAuth, asyncMiddleware( NotebookController.getNotebookList ))

router.post( '/', checkAuth, asyncMiddleware( NotebookController.createNewNotebook ))

router.delete( '/:id', checkAuth, asyncMiddleware( NotebookController.deleteNotebook ))

router.put( '/:id', checkAuth, asyncMiddleware( NotebookController.updateNotebook ))

module.exports = router
