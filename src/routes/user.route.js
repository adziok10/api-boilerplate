const express = require('express')

const asyncMiddleware = require('../lib/async-middleware.lib')
const UserController = require('../controllers/user.controller')
const checkAuth = require('../lib/auth-check.lib')
const validate = require('../lib/validate.lib')

const router = express.Router()

router.get( '/status', checkAuth, asyncMiddleware( UserController.getStatus ))

router.post( '/new', validate('user', 'register'), asyncMiddleware( UserController.register ))

router.post( '/', asyncMiddleware( UserController.login ))

router.delete( '/', checkAuth, asyncMiddleware( UserController.delete ))

router.put( '/', checkAuth, validate('user', 'update'), asyncMiddleware( UserController.update ))

// router.get( '/:id', asyncMiddleware( UserController.getInfo ))

// router.get( '/', asyncMiddleware( UserController.getInfo ))

module.exports = router
