module.exports = {
    register: {
        type: 'object',
        properties: {
            login: {
                type: "string",
                minLength: 5,
                maxLength: 20
            },
            password: {
                type: "string",
                minLength: 5,
                maxLength: 50
            },
            email: {
                type: "string",
                format: "email"
            }
        }
    },
    update: {
        type: 'object',
        properties: {
            email: {
                type: "string",
                format: "email"
            },
            password: {
                type: "string",
                minLength: 5,
                maxLength: 50
            }
        },
        anyOf: [{
                type: 'object',
                properties: {
                    email: {
                        type: "string",
                        format: "email"
                    }
                },
                required: ['email']
            },
            {
                type: 'object',
                properties: {
                    password: {
                        type: "string",
                        minLength: 5,
                        maxLength: 50
                    }
                },
                required: ['password']
            }
        ]
    }
}